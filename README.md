# README #

### What is this repository for? ###

This tool is designed to obtain nearest words from word2vec model based on the vobulary the model itself contain. One of the problem is to compute nearest words using the word2vec model is its higher computetional cost. So for example, if i want to obtain nearest words for the vocabulary the model itself contains 100 times it will compute the distance 100 times for the same word. Therefore, the purpose of this tool is that it will compute nearest words for every word in the vocabulary one time and save it in a file as a dictionary. Later this dictionary can be used for many different purpose to obtain nearest words for a particular word. 

* It reads model file. From which it generates nearest tokens for each word in the vocabulary.
* Currently it will list top 40 nearest words. Please change the number N if you want more or less.

##### Version #####
* Version 0.1

### How do I get set up? ###
We have different versions for linux and OS X platforms, please check the following:

* [OS X](https://bitbucket.org/firojalam/word2vecnearestwords/downloads/w2vnearesttok.mac.osx.tar)

* [Linux](https://bitbucket.org/firojalam/word2vecnearestwords/downloads/w2vnearesttok.linux.tar)

#### IDE for the project ####
* Netbeans IDE can be used to edit and compile. 
 
## How to run? ##
./w2vnearesttok <w2v-model-FILE> <output-FILE>

w2v-model-FILE: a model binary file that has been designed using word2vec (http://word2vec.googlecode.com/svn/trunk/)